//
//  DatePicker.swift
//  PotionUI
//
//  Created by Damar Paramartha on 09/02/21.
//

import SwiftUI

struct DatePickerView: View {
    @State private var date =  Date()
    
    
    var body: some View {
        VStack {
            AppBar(title: "Date Picker")
            VStack(alignment: .leading) {
                DatePicker(
                    "Start Date",
                    selection: $date,
                    displayedComponents: [.date]
                )
                .accentColor(Color.potSuccess)
                .datePickerStyle(GraphicalDatePickerStyle())
                Spacer()
                //BELUM BISA NAMPILIN DATE SESUAI KEINGINAN
                Text("\(date)")
            }
            .padding()
        }
        .navSpacer()
    }
}

struct DatePickerView_Previews: PreviewProvider {
    static var previews: some View {
        DatePickerView()
    }
}
