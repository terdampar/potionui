//
//  ContentView.swift
//  PotionUI
//
//  Created by Damar Paramartha on 22/10/20.
//

import SwiftUI

struct ContentView: View {
    
    
    var body: some View {
        
        NavigationView {
            TabView() {
                Component()
                    .tabItem {
                        Image(systemName: "square.stack.3d.up.fill")
                        Text("Components")
                    }
                Upcoming()
                    .tabItem {
                        Image(systemName: "arrow.forward")
                        Text("Coming Soon")
                    }
                Profile()
                    .navigationTitle("Profile")
                    .tabItem {
                        Image(systemName: "person")
                        Text("Mode")
                        
                    }
            }
        }
        .accentColor(Color.potSuccess)
        
    }
}

struct Component: View {
    
    @State var infoPresented : Bool = false
    
    var body: some View{
        ZStack {
            VStack(alignment: .leading, spacing: 0) {
                AppBarLarge(title: "PotionUI", backButton: .hide)
                ScrollView {
                    ScrollView(.horizontal) {
                        HStack (spacing: 0){
                            HStack {
                                Image(systemName: "sun.min")
                                Text("Dark Mode Support")
                            }
                            .onTapGesture(perform: {
                                infoPresented.toggle()
                            })
                            Divider()
                                .padding(.horizontal)
                            HStack {
                                Image(systemName: "slider.horizontal.3")
                                Text("Customizable")
                            }
                            Divider()
                                .padding(.horizontal)
                            HStack {
                                Image(systemName: "hand.thumbsup")
                                Text("Ez Documentation")
                            }
                            Divider()
                                .padding(.horizontal)
                            HStack {
                                Image(systemName: "star")
                                Text("Another BS")
                            }
                        }
                        .padding()
                        .potFont(.OpensansRegular16)
                    }
                    .background(Color.potSoftSecondary)
                    VStack(){
                        NavigationLink(destination: Buttons()){
                            ListNav(text: "Button, Color, Typography")
                        }
                        NavigationLink(destination: Card()){
                            ListNav(text: "Card")
                        }
                        NavigationLink(destination: Info()){
                            ListNav(text: "Info")
                        }
                        NavigationLink(destination: Input()){
                            ListNav(text: "Input")
                        }
                        NavigationLink(destination: List()){
                            ListNav(text: "List & View More")
                        }
                        NavigationLink(destination: Loading()){
                            ListNav(text: "Loading Animation")
                        }
                        NavigationLink(destination: Menus()){
                            ListNav(text: "Menus")
                        }
                        NavigationLink(destination: Modal()){
                            ListNav(text: "Modal")
                        }
                        
                        NavigationLink(destination: StepByStep()){
                            ListNav(text: "Step by Step")
                        }
                        
                    }
                }
            }
            
            .navSpacer()
            ViewInfo(
                isPresented: $infoPresented,
                title: "Dark Mode Support",
                description: "Informasi adalah pesan atau kumpulan pesan yang terdiri dari order sekuens dari simbol, atau makna yang dapat ditafsirkan dari pesan atau kumpulan pesan. Informasi dapat direkam atau ditransmisikan. Hal ini dapat dicatat sebagai tanda-tanda, atau sebagai sinyal berdasarkan gelombang."
            )
        }
        
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
//            .preferredColorScheme(.dark)
    }
}
