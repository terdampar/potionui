//
//  Selection.swift
//  PotionUI
//
//  Created by Damar Paramartha on 29/01/21.
//

import SwiftUI

struct Selection: View {
    @State var showingYear = false
    @State var showingMonth = false
    
    @State private var tahun = "Tahun"
    @State private var bulan = "Bulan"
    
    
    var body: some View {
        VStack {
            AppBar(title: "Selection")
            HStack {
                potButton(text: tahun, image: Image(systemName: "chevron.down"), style: .fill, textColor: .potBW, color: .potLight, action: {showingYear.toggle()})
                    .sheet(isPresented: $showingYear, content: {
                        Year(tahun: $tahun, showingYear: $showingYear)
                    })
                potButton(text: bulan, image: Image(systemName: "chevron.down"), style: .fill, textColor: .potBW, color: .potLight, action: {showingMonth.toggle()})
                    .sheet(isPresented: $showingMonth, content: {
                        Month(bulan: $bulan, showingMonth: $showingMonth)
                    })
            }
            .padding()
            
            Spacer()
        }
        .navSpacer()
    }
}

struct Year: View {
    @Binding var tahun: String
    @Binding var showingYear: Bool
    
    var body: some View{
        VStack {
            AppBar(title: "Pilih Tahun", backButton: .hide)
            VStack{
                SelectItem(text: "2021", value: "2021", group: $tahun)
                SelectItem(text: "2020", value: "2020", group: $tahun)
                SelectItem(text: "2019", value: "2019", group: $tahun)
                SelectItem(text: "2018", value: "2018", group: $tahun)
                SelectItem(text: "2017", value: "2017", group: $tahun)
                SelectItem(text: "2016", value: "2016", group: $tahun)
                Spacer()
                potButton(text: "Ok", style: .fill, textColor: .potBW, color: .potLight, action: {showingYear.toggle()})
            }
            .padding()
        }
    }
}
struct Month: View {
    @Binding var bulan: String
    @Binding var showingMonth: Bool
    
    var body: some View{
        VStack {
            AppBar(title: "Pilih Bulan", backButton: .hide)
            VStack{
                SelectItem(text: "Januari", value: "Januari", group: $bulan)
                SelectItem(text: "Februari", value: "Februari", group: $bulan)
                SelectItem(text: "Maret", value: "Maret", group: $bulan)
                SelectItem(text: "April", value: "April", group: $bulan)
                SelectItem(text: "Mei", value: "Mei", group: $bulan)
                SelectItem(text: "Juni", value: "Juni", group: $bulan)
                Spacer()
                potButton(text: "Ok", style: .fill, textColor: .potBW, color: .potLight, action: {showingMonth.toggle()})
            }
            .padding()
        }
    }
}

public struct SelectItem: View {
    public init(text: String, value: String, group: Binding<String>) {
        self.text = text
        self.value = value
        self._group = group
    }
    
    var text: String
    var value: String
    
    @State private var isSelected: Bool = false
    @Binding var group: String
    
    public var body: some View {
        VStack (spacing: 0){
            Button (action: {
                group = value
            }
            ) {
                HStack (alignment: .firstTextBaseline){
                    Text(text)
                        .potFont(.OpensansRegular16)
                        .foregroundColor(.potBW)
                    Spacer()
                    if(value == group){
                        Image(systemName: "checkmark")
                            .foregroundColor(.potSuccess)
                            .padding(.trailing)
                    }
                }
                
            }
            .padding(.vertical, 16)
            Divider()
        }
        
        
    }
}
struct Selection_Previews: PreviewProvider {
    static var previews: some View {
        Selection()
    }
}
