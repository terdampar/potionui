//
//  Colors.swift
//  potUI
//
//  Created by Damar Paramartha on 28/10/20.
//

import SwiftUI

//MARK: CUSTOM MODIFIER
extension Color {
    public static let potPrimary = Color("Primary", bundle: .module)
    public static let potSecondary = Color("Secondary", bundle: .module)
    public static let potSuccess = Color("Success", bundle: .module)
    public static let potWarning = Color("Warning", bundle: .module)
    public static let potDanger = Color("Danger", bundle: .module)
    public static let potInfo = Color("Info", bundle: .module)
    public static let potLight = Color("Light", bundle: .module)
    
    public static let potSoftPrimary = Color("PrimarySofter", bundle: .module)
    public static let potSoftSecondary = Color("SecondarySofter", bundle: .module)
    public static let potSoftSuccess = Color("SuccessSofter", bundle: .module)
    public static let potSoftWarning = Color("WarningSofter", bundle: .module)
    public static let potSoftDanger = Color("DangerSofter", bundle: .module)
    public static let potSoftInfo = Color("InfoSofter", bundle: .module)
    public static let potSoftLight = Color("LightSofter", bundle: .module)
    
    public static let potDarkPrimary = Color("PrimaryDarker", bundle: .module)
    public static let potDarkSecondary = Color("SecondaryDarker", bundle: .module)
    public static let potDarkSuccess = Color("SuccessDarker", bundle: .module)
    public static let potDarkWarning = Color("WarningDarker", bundle: .module)
    public static let potDarkDanger = Color("DangerDarker", bundle: .module)
    public static let potDarkInfo = Color("InfoDarker", bundle: .module)
    public static let potDarkLight = Color("LightDarker", bundle: .module)
    
    public static let potBW = Color("BW", bundle: .module)
    public static let potWB = Color("WB", bundle: .module)
    
}//: CUSTOM MODIFIER

struct Colors: View {
    var body: some View {
        VStack {
            AppBar(title: "Color")
            KindsOfColors()
            Spacer()
        }
        .navSpacer()
    }
}

struct KindsOfColors: View {
    var body: some View {
        VStack {
            ScrollView(.horizontal) {
                VStack {
                    Text("Action Color")
                        .potFont(.Montserrat16)
                    HStack {
                        Rectangle().frame(width: 50, height: 50)
                            .foregroundColor(.potPrimary)
                        Rectangle().frame(width: 50, height: 50)
                            .foregroundColor(.potSecondary)
                        Rectangle().frame(width: 50, height: 50)
                            .foregroundColor(.potSuccess)
                        Rectangle().frame(width: 50, height: 50)
                            .foregroundColor(.potWarning)
                        Rectangle().frame(width: 50, height: 50)
                            .foregroundColor(.potDanger)
                        Rectangle().frame(width: 50, height: 50)
                            .foregroundColor(.potInfo)
                        Rectangle().frame(width: 50, height: 50)
                            .foregroundColor(.potLight)
                    }
                    
                    Text("Softer Action Color")
                        .potFont(.Montserrat16)
                    HStack{
                        Rectangle().frame(width: 50, height: 50)
                            .foregroundColor(.potSoftPrimary)
                        Rectangle().frame(width: 50, height: 50)
                            .foregroundColor(.potSoftSecondary)
                        Rectangle().frame(width: 50, height: 50)
                            .foregroundColor(.potSoftSuccess)
                        Rectangle().frame(width: 50, height: 50)
                            .foregroundColor(.potSoftWarning)
                        Rectangle().frame(width: 50, height: 50)
                            .foregroundColor(.potSoftDanger)
                        Rectangle().frame(width: 50, height: 50)
                            .foregroundColor(.potSoftInfo)
                        Rectangle().frame(width: 50, height: 50)
                            .foregroundColor(.potSoftLight)
                    }
                    Text("Darker Action Color")
                        .potFont(.Montserrat16)
                    HStack{
                        Rectangle().frame(width: 50, height: 50)
                            .foregroundColor(.potDarkPrimary)
                        Rectangle().frame(width: 50, height: 50)
                            .foregroundColor(.potDarkSecondary)
                        Rectangle().frame(width: 50, height: 50)
                            .foregroundColor(.potDarkSuccess)
                        Rectangle().frame(width: 50, height: 50)
                            .foregroundColor(.potDarkWarning)
                        Rectangle().frame(width: 50, height: 50)
                            .foregroundColor(.potDarkDanger)
                        Rectangle().frame(width: 50, height: 50)
                            .foregroundColor(.potDarkInfo)
                        Rectangle().frame(width: 50, height: 50)
                            .foregroundColor(.potDarkLight)
                    }
                    Text("Accent Color")
                        .potFont(.Montserrat16)
                    HStack{
                        Rectangle().frame(width: 50, height: 50)
                            .foregroundColor(.potBW)
                        Rectangle().frame(width: 50, height: 50)
                            .foregroundColor(.potWB)
                    }
                }
            }
            Spacer()
        }
    }
}


struct Colors_Previews: PreviewProvider {
    static var previews: some View {
        Colors()
    }
}
