//
//  Radio.swift
//  PotionUI
//
//  Created by Damar Paramartha on 10/12/20.
//

import SwiftUI

struct Radio: View {
    
    
    @State private var binatang = "Anjing"
    @State private var burung = "Birds"
    
    var body: some View {
        VStack(spacing: 0) {
            AppBar(title: "Radio")
            ScrollView {
                VStack (spacing: 0){
                    VStack {
                        RadioItem(text: "A", value: "Anjing", group: $binatang)
                        RadioItem(text: "B", value: "Babi", group: $binatang)
                        RadioItem(text: "C", value: "Cicak", group: $binatang)
                        RadioItem(text: "D", value: "Dinosaurus", group: $binatang)
                        HStack{
                            Text("Pilihan Anda: ")
                            Spacer()
                            Text(binatang)
                        }
                    }
                    .padding(.bottom, 36)
                    VStack{
                        RadioItem(text: "M", value: "Myna", group: $burung)
                        RadioItem(text: "S", value: "Starling", group: $burung)
                        HStack{
                            Text("Pilihan Anda: ")
                            Spacer()
                            Text(burung)
                        }
                    }
                    Spacer()
                }
                .padding()
            }
        }
        .navSpacer()
    }
}

public struct RadioItem: View {
    public init(text: String, value: String, group: Binding<String>) {
        self.text = text
        self.value = value
        self._group = group
    }
    
    var text: String
    var value: String
    
    @State private var isSelected: Bool = false
    @Binding var group: String
    
    public var body: some View {
        VStack (spacing: 0){
            Button (action: {
                group = value
            }
            ) {
                HStack (alignment: .firstTextBaseline){
                    if(value == group){
                        Image(systemName: "largecircle.fill.circle")
                            .foregroundColor(.potSuccess)
                    }else{
                        Image(systemName: "circle")
                            .foregroundColor(.potSuccess)
                    }
                    Text(text)
                        .potFont(.OpensansRegular16)
                        .foregroundColor(.potBW)
                    Spacer()
                }
                
            }
            .padding(.vertical, 16)
            Divider()
        }
        
        
    }
}

struct Radio_Previews: PreviewProvider {
    static var previews: some View {
        Radio()
    }
}
