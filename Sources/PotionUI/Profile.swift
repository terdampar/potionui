//
//  Profile.swift
//  PotionUI
//
//  Created by Damar Paramartha on 03/12/20.
//

import SwiftUI

public struct ItemCopy: View {
    public init(image: String, title: String, desc: String) {
        self.image = image
        self.title = title
        self.desc = desc
    }
    
    var image: String
    var title: String
    var desc: String
    
    public var body: some View{
        VStack (alignment: .leading, spacing: 0){
            HStack (alignment: .center){
                Image(systemName: image)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 16, height: 16)
                    .foregroundColor(.potSecondary)
                Text(title)
                    .potFont(.OpensansBold16, color: .potSecondary)
            }
            VStack (alignment: .leading, spacing: 0){
                Button(action: {UIPasteboard.general.string = desc}, label: {
                    Text(desc)
                        .foregroundColor(Color.potBW)
                    Spacer()
                    Image(systemName: "doc.on.doc")
                        .foregroundColor(Color.potSecondary)
                })
                .padding()
                .background(Color.potLight)
                .cornerRadius(8)
                
            }
            .padding(.vertical)
            Divider()
        }
    }
}
public struct ItemDesc: View {
    public init(image: String, title: String, desc: String) {
        self.image = image
        self.title = title
        self.desc = desc
    }
    
    var image: String
    var title: String
    var desc: String
    
    public var body: some View{
        VStack (alignment: .leading, spacing: 0){
            HStack (alignment: .center){
                
                Image(systemName: image)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 16, height: 16)
                    .foregroundColor(.potSecondary)
                Text(title)
                    .potFont(.OpensansBold16, color: .potSecondary)
            }
            Spacer(minLength: 0)
            VStack (alignment: .leading, spacing: 0){
                Text(desc)
                    .potFont(.OpensansRegular16)
                Spacer(minLength: 0)
            }
            .padding(.vertical)
            Divider()
        }
    }
}

struct Profile: View {
    var body: some View {
        ZStack (alignment: .top){
            Image("artwork", bundle: .module)
                .resizable()
                .scaledToFit()
            ScrollView {
                VStack(alignment: .leading, spacing: 0) {
                    
                    VStack (alignment: .leading, spacing: 0){
                        HStack {
                            Text("Jakarta")
                                .potFont(.Montserrat32)
                            Spacer()
                            potButton(image: Image(systemName: "gearshape.fill"), style: .ghost, textColor: .potBW, color: .potBW, action: {})
                            
                        }
                        .padding(.bottom)
                        VStack(spacing: 16) {
                            ItemCopy(image: "number", title: "Nomor", desc: "desc123")
                            ItemDesc(image: "location", title: "Alamat", desc: "Jl. Pulo Lentut No. 10. Kawasan Industri Pulo Gadung. Jakarta, DKI Jakarta 13920")
                        }
                        
                    }
                    .padding()
                    .background(Color.potWB)
                }
                .cornerRadius(8)
                .padding(.top, 232)
            }
            
        }
        .ignoresSafeArea(edges: .top)
    }
}

struct Profile_Previews: PreviewProvider {
    static var previews: some View {
        Profile()
    }
}
