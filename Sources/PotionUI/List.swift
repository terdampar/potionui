//
//  List.swift
//  PotionUI
//
//  Created by Damar Paramartha on 26/11/20.
//

import SwiftUI

struct List: View {
    @State var isPresented : Bool = false
    
    var body: some View{
        ZStack {
            VStack (spacing: 0) {
                AppBar(title: "List", backButton: .show)
                ScrollView {
                    VStack (spacing: 0){
                        ListItem(
                            image: Image("artwork"),
                            title: "A Sky Full of Stars",
                            desc: "Coldplay", itsViewMore: $isPresented
                        )
                        ListItem(
                            image: Image("artwork"),
                            title: "Adventure of A Lifetime",
                            desc: "Coldplay", itsViewMore: $isPresented
                        )
                        ListItem(
                            image: Image("artwork"),
                            title: "Every Teardrop is a Waterfall",
                            desc: "Coldplay", itsViewMore: $isPresented
                        )
                        Spacer()
                    }
                }
            }
            .navSpacer()
            ViewMore(isPresented: $isPresented)
        }
    }
}

public struct ListItem: View {
    public init(image: Image, title: String, desc: String, itsViewMore: Binding<Bool>) {
        self.image = image
        self.title = title
        self.desc = desc
        self._itsViewMore = itsViewMore
    }
    
    var image: Image
    var title: String
    var desc: String
    
    @Binding var itsViewMore: Bool
    
    public var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            HStack (alignment: .center) {
                image
                    .resizable()
                    .frame(width: 48, height: 48)
                    .cornerRadius(4)
                VStack(alignment: .leading) {
                    Text(title)
                        .potFont(.OpensansSemibold16)
                        .aspectRatio(contentMode: .fit)
                    Text(desc)
                        .potFont(.OpensansRegular14)
                }
                Spacer(minLength: 0)
                Button(action: {itsViewMore.toggle()}){
                    Image(systemName: "ellipsis")
                }
            }
            .padding()
            Divider()
                .padding(.horizontal, 16)
        }
        .onLongPressGesture {
            itsViewMore.toggle()
        }
    }
}

struct List_Previews: PreviewProvider {
    static var previews: some View {
        List()
            .preferredColorScheme(.dark)
    }
}
