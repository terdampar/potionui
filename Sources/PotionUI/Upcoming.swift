//
//  Upcoming.swift
//  PotionUI
//
//  Created by Damar Paramartha on 15/12/20.
//

import SwiftUI

struct Upcoming: View {
    var body: some View {
        VStack {
            AppBarLarge(title: "Upcoming", backButton: .hide)
            Text("Shimmer Animation")
            Shimmer()
            Spacer()
        }
        .navSpacer()
    }
}

struct Upcoming_Previews: PreviewProvider {
    static var previews: some View {
        Upcoming()
    }
}
