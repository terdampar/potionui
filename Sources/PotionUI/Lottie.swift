//import SwiftUI
//import Lottie
//
//struct LottieView: UIViewRepresentable {
//    typealias UIViewType = UIView
//    var filename: String
//
//    func makeUIView(context: UIViewRepresentableContext<LottieView>) -> UIView {
//        let view = UIView()
//
//        let animationView = AnimationView()
//        let animation = Animation.named(filename)
//        animationView.animation = animation
//        animationView.loopMode = .loop
//        animationView.contentMode = .scaleAspectFit
//        animationView.play()
//
//        animationView.translatesAutoresizingMaskIntoConstraints = false
//        view.addSubview(animationView)
//
//        NSLayoutConstraint.activate([
//            animationView.widthAnchor.constraint(equalTo: view.widthAnchor),
//            animationView.heightAnchor.constraint(equalTo: view.heightAnchor)
//        ])
//
//        return view
//    }
//
//    func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<LottieView>) {
//
//    }
//}
//
//
//struct LottieAnimation: View {
//    var body: some View {
//        VStack {
//            LottieView(filename: "EPM")
//                .frame(width: 100, height: 100)
//        }
//
//    }
//}
//
//struct LottieAnimation_Previews: PreviewProvider {
//    static var previews: some View {
//        LottieAnimation()
//    }
//}
