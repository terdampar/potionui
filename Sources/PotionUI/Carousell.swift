//
//  Carousell.swift
//  PotionUI
//
//  Created by Damar Paramartha on 09/02/21.
//

import SwiftUI

struct CarousellView: View {
    
    var body: some View {
        VStack {
            AppBar(title: "Carousell")
            VStack {
                Carousell{
                    ZStack (alignment: .topTrailing) {
                        ZStack {
                            Rectangle()
                                .frame(width: .infinity, height: 250)
                                .foregroundColor(.potSoftInfo)
                            Image("message", bundle: .module)
                                .resizable()
                                .scaledToFit()
                                .padding()
                        }
                        Text("Some Text")
                            .potFont(.OpensansSemibold16, color: .white)
                            .padding(.horizontal)
                            .padding(4)
                            .background(Color.black.opacity(0.38))
                            .cornerRadius(8)
                            .padding()
                    }
                    VStack {
                        Text("Something in the way")
                    }
                    VStack {
                        Text("She moves")
                    }
                }
            }
            .padding()
            Spacer()
        }
        .navSpacer()
    }
}

public struct Carousell <Content : View> : View {
    var content : Content
    
    public init(@ViewBuilder content: () -> Content) {
        self.content = content()
    }
    public var body: some View {
        VStack {
            TabView {
                content
            }
            .frame(width: .infinity, height: 250, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .tabViewStyle(PageTabViewStyle(indexDisplayMode: .always))
            .background(Color.potLight)
            .cornerRadius(8)
        }
        .shadow(color: Color.black.opacity(0.1), radius: 24, x: 0, y: 8)
    }
}

struct Carousell_Previews: PreviewProvider {
    static var previews: some View {
        CarousellView()
    }
}
