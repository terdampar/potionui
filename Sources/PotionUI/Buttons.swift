//
//  Buttons.swift
//  potUI
//
//  Created by Damar Paramartha on 28/10/20.
//

import SwiftUI

//MARK: CUSTOM MODIFIER
public struct potButtonStyle: ButtonStyle {
    public init(textColor: Color, color: Color, style: potButton.Style) {
        self.textColor = textColor
        self.color = color
        self.style = style
    }
    
    var textColor: Color
    var color: Color
    var style: potButton.Style
    
    public func makeBody(configuration: ButtonStyle.Configuration) -> some View {
        switch style {
        case .fill: return AnyView(FillButton(textColor: textColor, color: color, configuration: configuration))
        case .outline: return AnyView(OutlineButton(color: color, configuration: configuration))
        case .ghost: return AnyView(GhostButton(textColor: textColor,  configuration: configuration))
        }
    }
    
    public struct FillButton: View {
        public init(textColor: Color, color: Color, configuration: ButtonStyleConfiguration) {
            self.textColor = textColor
            self.color = color
            self.configuration = configuration
        }
        
        var textColor: Color
        var color: Color
        let configuration: ButtonStyle.Configuration
        public var body: some View {
            configuration.label
                .potFont(.OpensansBold16)
                .foregroundColor(textColor)
                .padding(.vertical, 8)
                .padding(.horizontal, 16)
                .frame(minHeight: 56)
                .background(color)
                .cornerRadius(8)
                .opacity(configuration.isPressed ? 0.7 : 1)
        }
    }
    
    public struct OutlineButton: View {
        public init(color: Color, configuration: ButtonStyleConfiguration) {
            self.color = color
            self.configuration = configuration
        }
        
        var color: Color
        let configuration: ButtonStyle.Configuration
        public var body: some View {
            configuration.label
                .potFont(.OpensansBold16)
                .foregroundColor(color)
                .padding(.vertical, 8)
                .padding(.horizontal, 16)
                .frame(minHeight: 56)
                .cornerRadius(8)
                .overlay(
                    RoundedRectangle(cornerRadius: 8)
                        .stroke(color, lineWidth: 2)
                )
                .opacity(configuration.isPressed ? 0.7 : 1)
        }
    }
    
    public struct GhostButton: View {
        public init(textColor: Color, configuration: ButtonStyleConfiguration) {
            self.textColor = textColor
            self.configuration = configuration
        }
        
        var textColor: Color
        let configuration: ButtonStyle.Configuration
        public var body: some View {
            configuration.label
                .potFont(.OpensansBold16)
                .foregroundColor(textColor)
                .opacity(configuration.isPressed ? 0.7 : 1)
        }
    }
}
public struct potButton: View {
    public init(text: String? = nil, image: Image? = nil, style: potButton.Style = .fill, textColor: Color? = nil, color: Color = .potPrimary, isLoading: Bool = false, action: @escaping () -> Void) {
        self.text = text
        self.image = image
        self.style = style
        self.textColor = textColor
        self.color = color
        self.isLoading = isLoading
        self.action = action
    }
    
    
    public enum Style {
        case fill, outline, ghost
    }
    
    var text: String?
    var image: Image?
    var style: Style = .fill
    var textColor: Color?
    var color: Color = .potPrimary
    var isLoading: Bool = false
    var action: () -> Void
    var textAndImage: Bool { text != nil && image != nil }
    
    @State private var shouldAnimate = false
    
    public var body: some View {
        Button(action: action, label: {
            HStack() {
                if(style != .ghost){
                    Spacer()
                }
                if isLoading {
                    HStack {
                        Group {
                            Circle()
                                .scaleEffect(shouldAnimate ? 1.0 : 0.5)
                                .animation(Animation.easeInOut(duration: 0.5).repeatForever())
                            Circle()
                                .scaleEffect(shouldAnimate ? 1.0 : 0.5)
                                .animation(Animation.easeInOut(duration: 0.5).repeatForever().delay(0.3))
                            Circle()
                                .scaleEffect(shouldAnimate ? 1.0 : 0.5)
                                .animation(Animation.easeInOut(duration: 0.5).repeatForever().delay(0.6))
                        }
                        .foregroundColor(textColor)
                        .frame(width: 8, height: 8)
                    }
                    .onAppear {
                        shouldAnimate = true
                    }
                } else{
                    
                    HStack(spacing: textAndImage ? 12 : 0) {
                        Text(text ?? "")
                            .lineLimit(1)
                        image
                    }
                }
                if(style != .ghost){
                    Spacer()
                }
            }
            
        })
        .style(style, color: color, textColor: textColor ?? .white)
        .disabled(isLoading)
        .opacity(isLoading ? 0.7 : 1)
    }
}

public extension Button {
    /// Changes the appearance of the button
    func style(_ style: potButton.Style, color: Color, textColor: Color) -> some View {
        self.buttonStyle(potButtonStyle(textColor: textColor, color: color, style: style))
    }
}
//: CUSTOM MODIFIER

struct Buttons: View {
    var body: some View {
        
        VStack {
            ScrollView {
                AppBar(title: "Button, Color, Typography")
                
                VStack {
                    potButton(
                        text: "Button B",
                        style: .outline,
                        color: .potDanger,
                        action: {print("Test")}
                    )
                    HStack {
                        potButton(
                            text: "Cancel",
                            style: .fill,
                            textColor: .potDanger,
                            color: .potLight,
                            action: {print("test")}
                        )
                        potButton(text: "Proceed",
                                  style: .fill,
                                  textColor: .white,
                                  color: .potSuccess,
                                  action: {print("Test")}
                        )
                    }
                    potButton(
                        text: "Ghost",
                        style: .ghost,
                        textColor: .potInfo,
                        action: {print("Test")}
                    )
                    Spacer()
                    Divider()
                        .padding()
                    KindsOfColors()
                    Divider()
                        .padding()
                    KindsOfTypography()
                    
                }
                .padding(.horizontal, 16)
            }
            .navSpacer()
        }
    }
}

struct Buttons_Previews: PreviewProvider {
    static var previews: some View {
        Buttons()
    }
}
