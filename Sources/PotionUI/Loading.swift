//
//  Loading.swift
//  PotionUI
//
//  Created by Damar Paramartha on 09/02/21.
//

import SwiftUI

struct Loading: View {
    @State private var isLoading = false
    
    var body: some View {
        VStack{
            AppBar(title: "Loading Animation")
            VStack {
                potButton(
                    text: "Some action",
                    textColor: .white,
                    isLoading: isLoading,
                    action: {isLoading.toggle()}
                )
//                LottieView(filename: "EPM")
//                    .frame(width: 100, height: 100)
                Spacer()
                potButton(
                    text: "Reset Animation",
                    textColor: Color.potDanger,
                    color: Color.potLight,
                    action: {isLoading = false}
                )
            }
            .padding()
        }
        .navSpacer()
    }
}

struct Loading_Previews: PreviewProvider {
    static var previews: some View {
        Loading()
    }
}
