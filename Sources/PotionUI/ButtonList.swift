//
//  ButtonList.swift
//  PotionUI
//
//  Created by Damar Paramartha on 03/12/20.
//

import SwiftUI



public struct ButtonList: View {
    public init(action: () -> Void, icon: Image? = nil, text: String, color: Color? = nil) {
        self.action = {}
        self.icon = icon
        self.text = text
        self.color = color
    }
    
    
    var action: () -> Void
    var icon : Image?
    var text : String
    var color : Color?
    
    public var body: some View {
        VStack(alignment: .leading, spacing: 0){
            HStack (alignment: .firstTextBaseline, spacing: nil){
                Button(action: action, label: {
                    icon
                        .frame(width: 16, height: 16, alignment: .center)
                        .scaledToFit()
                        .padding(.trailing, 8)
                    Text(text)
                        .potFont(.OpensansSemibold16)
                    Spacer()
                })
            }
            .foregroundColor(color ?? .potSecondary)
            .padding(.vertical, 16)
            Divider()
        }
    }
}

struct ButtonList_Previews: PreviewProvider {
    static var previews: some View {
        ButtonList(action: {}, icon: Image(systemName: "trash"), text: "Nice")
            .preferredColorScheme(.dark)
            .padding()
            .previewLayout(.sizeThatFits)
    }
}
