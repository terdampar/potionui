//
//  Input.swift
//  PotionUI
//
//  Created by Damar Paramartha on 03/02/21.
//

import SwiftUI

struct Input: View {
    var body: some View {
            VStack{
                AppBar(title: "Input")
                Image("message")
                    .resizable()
                    .scaledToFit()
                    .frame(width: .infinity, height: 100)
                NavigationLink(destination: CheckBox()){
                    ListNav(text: "CheckBox")
                }
                NavigationLink(destination: InputText()){
                    ListNav(text: "Input Text")
                }
                NavigationLink(destination: Radio()){
                    ListNav(text: "Radio")
                }
                NavigationLink(destination: Selection()){
                    ListNav(text: "Selection")
                }
                Spacer()
            }
            .navSpacer()
    }
}

struct Input_Previews: PreviewProvider {
    static var previews: some View {
        Input()
    }
}
