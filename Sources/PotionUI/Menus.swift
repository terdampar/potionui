//
//  Menus.swift
//  PotionUI
//
//  Created by Damar Paramartha on 24/11/20.
//

import SwiftUI

struct Menus: View {
    var columns : [GridItem]     = [
        GridItem(.flexible(minimum: 50, maximum: 120)),
        GridItem(.flexible(minimum: 50, maximum: 120)),
        GridItem(.flexible(minimum: 50, maximum: 120))
    ]
    
    var body: some View {
        VStack(spacing: 0) {
            AppBarLarge(title: "Menu")
            ScrollView {
                VStack(alignment: .leading, spacing: 0) {
                    LazyVGrid(columns: columns, spacing: 16){
                        MenuItem(
                            image: Image(systemName: "newspaper"),
                            text: "Newspaper",
                            badge: "1",
                            action: {}
                        )
                        MenuItem(
                            image: Image(systemName: "doc.text"),
                            text: "Document",
                            badge: "20",
                            action: {}
                        )
                        MenuItem(
                            image: Image(systemName: "calendar"),
                            text: "Calendar",
                            badge: "8",
                            action: {}
                        )
                        MenuItem(
                            image: Image(systemName: "bookmark"),
                            text: "Bookmark",
                            badge: "12",
                            action: {}
                        )
                        MenuItem(
                            image: Image(systemName: "person"),
                            text: "User",
                            action: {}
                        )
                        MenuItem(
                            image: Image(systemName: "pianokeys"),
                            text: "Music",
                            action: {}
                        )
                        MenuItem(
                            image: Image(systemName: "applelogo"),
                            text: "Swift UI",
                            action: {}
                        )
                    }
                }
                .padding(16)
                Spacer()
                VStack {
                    MenuListItem(
                        image: Image(systemName: "checkmark.shield.fill"),
                        text: "Swift UI",
                        action: {}
                    )
                MenuListItem(
                    image: Image(systemName: "wand.and.stars"),
                    text: "Swift UI",
                    action: {}
                )
                }
                    .padding()
            }
        }
        .navSpacer()
    }
}

public struct MenuItem: View{
    public init(image: Image, text: String, badge: String? = nil, action: @escaping () -> ()) {
        self.image = image
        self.text = text
        self.badge = badge
        self.action = action
    }
    
    var image: Image
    var text: String
    var badge: String?
    var action: () -> ()
    
    public var body: some View{
        Button(action: action){
            VStack (alignment: .center, spacing: 0) {
                ZStack {
                    Rectangle()
                        .aspectRatio(contentMode: .fit)
                        .foregroundColor(Color.potLight)
                        .cornerRadius(8)
                    image
                        .resizable()
                        .scaledToFit()
                        .frame(width: 36, height: 36, alignment: .center)
                        .foregroundColor(.potSecondary)
                    if(badge != nil){
                        Text(badge ?? "")
                            .potFont(.OpensansRegular14)
                            .padding(6)
                            .frame(maxWidth: 32)
                            .background(Color.potDanger)
                            .clipShape(Circle())
                            .foregroundColor(.white)
                            .offset(x: 20, y: -20)
                    }
                    
                    
                }
                Text(text)
                    .potFont(.OpensansRegular16)
            }
        }
        .foregroundColor(.potBW)
    }
}

public struct MenuListItem: View{
    internal init(image: Image, text: String, action: @escaping () -> ()) {
        self.image = image
        self.text = text
        self.action = action
    }
    
    var image: Image
    var text: String
    var action: () -> ()
    
    public var body: some View{
        Button(action: action){
            VStack (spacing: 0){
                HStack (alignment: .center) {
                    ZStack {
                        Rectangle()
                            .frame(width: 28, height: 28, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .foregroundColor(Color.potInfo)
                            .cornerRadius(4)
                        image
                            .resizable()
                            .scaledToFit()
                            .frame(width: 16, height: 16, alignment: .center)
                            .foregroundColor(.white)
                        
                    }
                    Text(text)
                        .potFont(.OpensansRegular16)
                    Spacer()
                    Image(systemName: "chevron.right")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 16, height: 16, alignment: .center)
                        .foregroundColor(.potSecondary)
                }
                .padding(.vertical)
                Divider()
            }
        }
        .foregroundColor(.potBW)
        
    }
}
struct Menus_Previews: PreviewProvider {
    static var previews: some View {
        Menus()
        
    }
}
