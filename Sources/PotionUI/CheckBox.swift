//
//  CheckBox.swift
//  PotionUI
//
//  Created by Damar Paramartha on 10/12/20.
//

import SwiftUI


struct CheckBox: View {
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>

    @State private var checkA: Bool = false
    @State private var checkB: Bool = false

    var body: some View{
        VStack {
            AppBar(title: "CheckBox", backButton: .hide)
            VStack {
                CheckBoxItem(text: "CheckBox A", isSelected: $checkA)
                CheckBoxItem(text: "CheckBox B", isSelected: $checkB)
                VStack (spacing: 16) {
                HStack {
                    Text("Checkbox A: ")
                    Spacer()
                    Text(checkA ? "Checked" : "Unchecked")
                }
                HStack {
                    Text("Checkbox B: ")
                    Spacer()
                    Text(checkB ? "Checked" : "Unchecked")
                }
            }
            }
            .padding(.horizontal)
            .potFont(.OpensansRegular16)
            Spacer()
            Text("*Disabling back button*")
                .potFont(.OpensansRegular14)
            potButton(text: "Back", style: .fill, textColor: .potBW, color: .potSoftDanger, action: {self.mode.wrappedValue.dismiss()})
                .padding()
        }
        .navSpacer()
        
    }
}

public struct CheckBoxItem: View {
    public init(text: String, isSelected: Binding<Bool>) {
        self.text = text
        self._isSelected = isSelected
    }
    
    
    var text: String
    
    @Binding var isSelected: Bool
    
    public var body: some View {
        VStack (spacing: 0){
            Button (action: {
                isSelected.toggle()
            }) {
                HStack (alignment: .firstTextBaseline){
                    if(isSelected){
                        Image(systemName: "checkmark.square.fill")
                            .foregroundColor(.potSuccess)
                    }else{
                        Image(systemName: "square")
                            .foregroundColor(.potSuccess)
                    }
                    Text(text)
                        .potFont(.OpensansRegular16)
                        .foregroundColor(.potBW)
                    Spacer()
                }
            }
            .padding(.vertical, 16)
            Divider()
        }
    }
}

struct CheckBox_Previews: PreviewProvider {
    static var previews: some View {
        CheckBox()
    }
}
