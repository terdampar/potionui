//
//  Modal.swift
//  PotionUI
//
//  Created by Damar Paramartha on 12/11/20.
//

import SwiftUI


struct Modal: View {
    @State var isPresentedML : Bool = false
    @State var isPresentedMM : Bool = false
    @State var isPresentedMS : Bool = false
    @State var isPresentedPU : Bool = false
    
    var body: some View {
        ZStack {
            VStack (spacing: 0){
                
                AppBar(title: "Modal")
                
                VStack {
                    potButton(text: "Modal Large", style: .fill, textColor: .potPrimary, color: .potLight, action: {isPresentedML.toggle()})
                    potButton(text: "Modal Medium", style: .fill, textColor: .potPrimary, color: .potLight, action: {isPresentedMM.toggle()})
                    potButton(text: "Modal Small", style: .fill, textColor: .potPrimary, color: .potLight, action: {isPresentedMS.toggle()})
                    potButton(text: "Pop Up", style: .fill, textColor: .potPrimary, color: .potLight, action: {isPresentedPU.toggle()})
                }
                .padding()
                Spacer()
            }
            ModalLarge(
                title: "Title",
                image: Image("message"),
                text: "Pesan konfirmasi dengan pertanyaan yang cukup panjang atau panjang banget, cocok juga untuk ilustrasi.",
                secAction: {},
                secText: "Secondary",
                priAction: {},
                priText: "Primary", isPresentedML: $isPresentedML
            )
            ModalMedium(
                title: "Konfirmasi",
                text: "Pesan cepat dengan pertanyaan konfirmasi yang agak panjang, dan tidak pakai ilustrasi.",
                secAction: {},
                secText: "Cancel",
                priAction: {},
                priText: "Proceed",
                isPresentedMM: $isPresentedMM
            )
            ModalSmall(
                title: "Konfirmasi",
                text: "Pesan konfirmasi dengan pertanyaan singkat.",
                secAction: {isPresentedMS.toggle()},
                priAction: {},
                isPresentedMS: $isPresentedMS
            )
            PopUpView(
                image: "wifi.exclamationmark",
                desc: "Connection Problem",
                action: {},
                text: "RETRY",
                isPresentedPU: $isPresentedPU
            )
            
        }
        .ignoresSafeArea(.all)
        .navSpacer()
    }
}
public struct ModalLarge: View {
    public init(title: String, image: Image, text: String? = nil, secAction: @escaping () -> Void, secText: String, priAction: @escaping () -> Void, priText: String, isPresentedML: Binding<Bool>) {
        self.title = title
        self.image = image
        self.text = text
        self.secAction = secAction
        self.secText = secText
        self.priAction = priAction
        self.priText = priText
        self._isPresentedML = isPresentedML
    }
    
    var title: String
    var image: Image
    var text: String?
    var secAction: () -> Void
    var secText: String
    var priAction: () -> Void
    var priText: String
    
    @Binding var isPresentedML : Bool
    
    public var body: some View {
        ZStack {
            Rectangle()
                .foregroundColor(Color.potBW.opacity(0.2))
                .ignoresSafeArea(.all)
                .opacity(isPresentedML ? 1 : 0)
                .onTapGesture {
                    isPresentedML.toggle()
                }
                .animation(.easeInOut(duration: 0.3))
            VStack(spacing: 0) {
                Spacer()
                HStack(){
                    Image(systemName: "exclamationmark.triangle")
                    Text(title)
                        .potFont(.Montserrat20)
                    Spacer()
                    potButton(
                        image: Image(systemName: "xmark"),
                        style: .ghost,
                        textColor: .potSecondary,
                        action: {isPresentedML.toggle()}
                    )
                }
                .padding(16)
                .background(Color.potSoftLight)
                .cornerRadius(radius: 8, corners: [.topLeft, .topRight])
                VStack(spacing: 0){
                    image
                        .resizable()
                        .scaledToFit()
                        .padding(.bottom, 16)
                    Text(text ?? "")
                        .foregroundColor(Color.potSecondary)
                        .potFont(.OpensansRegular16)
                        .padding(.bottom, 32)
                    HStack {
                        potButton(
                            text: secText,
                            style: .fill,
                            textColor: .potDanger,
                            color: .potLight,
                            action: secAction)
                        potButton(
                            text: priText,
                            style: .fill,
                            textColor: .white,
                            color: .potSuccess,
                            action: priAction)
                    }
                }
                .padding(.bottom, 24)
                .padding(16)
                .background(Color.potSoftLight)
            }
            .offset(y: isPresentedML ? 0 : UIScreen.screenHeight)
        }
        .animation(.easeInOut(duration: 0.3))
    }
}

public struct ModalMedium: View{
    public init(title: String, text: String? = nil, secAction: @escaping () -> Void, secText: String, priAction: @escaping () -> Void, priText: String, isPresentedMM: Binding<Bool>) {
        self.title = title
        self.text = text
        self.secAction = secAction
        self.secText = secText
        self.priAction = priAction
        self.priText = priText
        self._isPresentedMM = isPresentedMM
    }
    
    var title: String
    var text: String?
    var secAction: () -> Void
    var secText: String
    var priAction: () -> Void
    var priText: String
    
    @Binding var isPresentedMM : Bool
    
    public var body: some View {
        
        ZStack {
            Rectangle()
                .foregroundColor(Color.potBW.opacity(0.2))
                .ignoresSafeArea(.all)
                .opacity(isPresentedMM ? 1 : 0)
                .onTapGesture {
                    isPresentedMM.toggle()
                }
                .animation(.easeInOut(duration: 0.2))
            VStack(spacing: 0){
                HStack(){
                    Image(systemName: "exclamationmark.triangle")
                    Text(title)
                        .potFont(.Montserrat20)
                    Spacer()
                    potButton(
                        image: Image(systemName: "xmark"),
                        style: .ghost,
                        textColor: .potSecondary,
                        action: {isPresentedMM.toggle()}
                    )
                }
                .padding(.bottom, 16)
                HStack {
                    Text(text ?? "")
                        .foregroundColor(Color.potSecondary)
                        .potFont(.OpensansRegular16)
                        .padding(.bottom, 32)
                }
                HStack {
                    potButton(text: secText, style: .fill, textColor: .potDanger, color: .potLight, action: secAction)
                    potButton(text: priText, style: .fill, textColor: .white, color: .potSuccess, action: priAction)
                }
            }
            .padding(16)
            .background(Color.potSoftLight)
            .cornerRadius(8.0)
            .padding(16)
            .shadow(color: Color.potBW.opacity(0.1), radius: 24, x: 0, y: 8)
        }
        .opacity(isPresentedMM ? 1 : 0)
        .animation(.easeInOut(duration: 0.3))
    }
}

public struct ModalSmall: View{
    enum Theme {
        case success, warning, danger
    }
    
    var title: String
    var text: String?
    //var theme: Theme
    var secAction: () -> Void
    var priAction: () -> Void
    
    @Binding var isPresentedMS : Bool
    
    public var body: some View {
        
        ZStack {
            Rectangle()
                .foregroundColor(Color.potBW.opacity(0.2))
                .ignoresSafeArea(.all)
                .opacity(isPresentedMS ? 1 : 0)
                .onTapGesture {
                    isPresentedMS.toggle()
                }
                .animation(.easeInOut(duration: 0.3))
            VStack {
                Spacer()
                VStack(alignment: .leading, spacing: 16){
                    HStack(){
                        Image(systemName: "exclamationmark.triangle")
                        Text(title)
                            .potFont(.Montserrat20)
                    }
                    HStack {
                        Text(text ?? "")
                            .potFont(.OpensansRegular16)
                            .foregroundColor(Color.potSecondary)
                        Spacer()
                        HStack {
                            Button(action: secAction, label: {
                                Image(systemName: "multiply")
                                    .resizable()
                                    .scaledToFill()
                                    .padding()
                                    .foregroundColor(Color.potDanger)
                                    .frame(width: 50, height: 50, alignment: .center)
                            })
                            .background(Color.potLight)
                            .clipShape(Circle())
                            
                            Button(action: priAction, label: {
                                Image(systemName: "checkmark")
                                    .resizable()
                                    .scaledToFill()
                                    .padding()
                                    .foregroundColor(Color.potSuccess)
                                    .frame(width: 50, height: 50, alignment: .center)
                            })
                            .background(Color.potLight)
                            .clipShape(Circle())
                        }
                        
                    }
                    
                }
                .padding(16)
                .background(LinearGradient(gradient: Gradient(colors: [.potSoftSuccess, .potSoftLight]), startPoint: /*@START_MENU_TOKEN@*/.leading/*@END_MENU_TOKEN@*/, endPoint: /*@START_MENU_TOKEN@*/.trailing/*@END_MENU_TOKEN@*/))
                .cornerRadius(8.0)
                .padding(16)
                .shadow(color: Color.potBW.opacity(0.1), radius: 24, x: 0, y: 8)
                .padding(.bottom, 24)
            }
            .offset(y: isPresentedMS ? 0 : UIScreen.screenHeight)
        }
        
        .animation(.easeInOut(duration: 0.3))
    }
}

public struct PopUpView: View {
    public init(image: String, desc: String, action: @escaping () -> (), text: String, isPresentedPU: Binding<Bool>) {
        self.image = image
        self.desc = desc
        self.action = action
        self.text = text
        self._isPresentedPU = isPresentedPU
    }
    
    var image: String
    var desc : String
    var action : () -> ()
    var text : String
    
    @Binding var isPresentedPU: Bool
    public var body: some View {
        VStack {
            Spacer()
            HStack {
                Image(systemName: image)
                Text(desc)
                Spacer()
                Button(action: action, label: {
                    Text(text)
                        .foregroundColor(.potSecondary)
                })
            }
            .potFont(.OpensansSemibold16)
            .padding(.horizontal, 16)
            .frame(height: 54)
            .background(LinearGradient(gradient: Gradient(colors: [.potSoftWarning, .potSoftLight]), startPoint: /*@START_MENU_TOKEN@*/.leading/*@END_MENU_TOKEN@*/, endPoint: /*@START_MENU_TOKEN@*/.trailing/*@END_MENU_TOKEN@*/))
            .cornerRadius(8)
            .shadow(color: Color("Shadow").opacity(0.38), radius: 24, x: 0, y: 8)
            .padding(.bottom, 24)
            .padding(16)
        }
        .offset(y: isPresentedPU ? 0 : UIScreen.screenHeight)
        .animation(.easeInOut(duration: 0.3))
    }
}

struct Modal_Previews: PreviewProvider {
    static var previews: some View {
        Modal()
            .preferredColorScheme(.light)
    }
}
