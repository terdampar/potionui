//
//  Progress Bar.swift
//  PotionUI
//
//  Created by Damar Paramartha on 05/02/21.
//

import SwiftUI

struct ProgressBarView: View {
    @State var progress: CGFloat = 10
    
    var body: some View {
        VStack {
            AppBar(title:"Progress Bar")
            VStack (alignment: .leading){
                Text("Progress Bar")
                    .potFont(.OpensansSemibold16, color: .potSuccess)
                ProgressBar(progress: $progress)
                Spacer()
                HStack {
                    potButton(
                        text: "- 10",
                        textColor: .white,
                        action: {
                            if (progress-10 < 0){
                                progress = 0
                            } else {
                                progress -= 10
                            }
                        }
                    )
                    potButton(text: "+ 10", action: {
                        if (progress+10 > 100){
                            progress = 100
                        } else {
                            progress += 10
                        }
                    }
                    )
                }
            }
            .padding()
        }
        .navSpacer()
    }
}

public struct ProgressBar: View {
    public init(progress: Binding<CGFloat>) {
        self._progress = progress
    }
    @Binding var progress: CGFloat
    
    public var body: some View{
        ZStack (alignment: .leading) {
            Rectangle()
                .frame(width: UIScreen.screenWidth-32, height: 38)
                .foregroundColor(.potLight)
                .cornerRadius(4)
            Rectangle()
                .frame(width: progress/100 * (UIScreen.screenWidth-32), height: 38)
                .foregroundColor(.potSuccess)
                .cornerRadius(4)
                .animation(.easeInOut(duration: 0.3))
        }
    }
}

struct Progress_Bar_Previews: PreviewProvider {
    static var previews: some View {
        ProgressBarView()
    }
}
