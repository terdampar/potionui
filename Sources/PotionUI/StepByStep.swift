//
//  StepByStep.swift
//  PotionUI
//
//  Created by Damar Paramartha on 29/01/21.
//

import SwiftUI

struct StepByStep: View {
    
    
    
    var body: some View {
        VStack (alignment: .leading){
            AppBar(title: "Step by Step")
            ScrollView {
                VStack (alignment: .leading, spacing: 24){
                    ZStack (alignment: .leading) {
                        VStack (alignment: .center, spacing: 0){
                            Text("1")
                                .potFont(.OpensansRegular14)
                                .padding()
                                .foregroundColor(Color.potBW)
                                .frame(width: 38, height: 38, alignment: .center)
                                .background(Color.potLight)
                                .clipShape(Circle())
                            RoundedRectangle(cornerRadius: .infinity)
                                .frame(width: 2, height: .infinity)
                                .foregroundColor(.potSoftSecondary)
                            Image(systemName: "chevron.down")
                                .potFont(.OpensansSemibold16)
                                .foregroundColor(Color.potSoftSecondary)
                        }
                        
                        VStack (alignment: .leading, spacing: 0){
                            Text("Suatu Proses")
                                .potFont(.OpensansSemibold20)
                                .padding(.bottom, 8)
                            Text("Deskripsi dari suatu proses yang mungkin bisa agak panjang atau panjang panjang panjang panjang sekali, garis disamping tetap mengikuti panjang baris.")
                                .potFont(.OpensansRegular16)
                        }
                        .padding(.leading, 46)
                        .padding(.top, 5)
                    }
                    ZStack (alignment: .leading) {
                        VStack (alignment: .center, spacing: 0){
                            Text("2")
                                .potFont(.OpensansRegular14)
                                .padding()
                                .foregroundColor(Color.white)
                                .frame(width: 38, height: 38, alignment: .center)
                                .background(Color.potSuccess)
                                .clipShape(Circle())
                            RoundedRectangle(cornerRadius: .infinity)
                                .frame(width: 2, height: .infinity)
                                .foregroundColor(.potSoftSecondary)
                            Image(systemName: "chevron.down")
                                .potFont(.OpensansSemibold16)
                                .foregroundColor(Color.potSoftSecondary)
                        }
                        
                        VStack (alignment: .leading, spacing: 0){
                            Text("Proses Lanjutan")
                                .foregroundColor(.potSuccess)
                                .potFont(.OpensansSemibold20)
                                .padding(.bottom, 8)
                            Text("Deskripsi dari suatu proses yang mungkin bisa agak panjang.")
                                .fixedSize(horizontal: false, vertical: true)
                                .potFont(.OpensansRegular16)
                            Spacer()
                            potButton(text: "Suatu Button", style: .fill, textColor: .white, color: .potSuccess, action: {})
                                .padding(.top, 8)
                            
                        }
                        .padding(.leading, 46)
                        .padding(.top, 5)
                    }
                    ZStack (alignment: .leading) {
                        VStack (alignment: .leading, spacing: 0){
                            Text("3")
                                .potFont(.OpensansRegular14)
                                .padding()
                                .foregroundColor(Color.potBW)
                                .frame(width: 38, height: 38, alignment: .center)
                                .background(Color.potLight)
                                .clipShape(Circle())
                            VStack {
                                RoundedRectangle(cornerRadius: .infinity)
                                    .frame(width: 2, height: .infinity)
                                    .foregroundColor(.potSoftSecondary)
                            Image(systemName: "chevron.down")
                                .potFont(.OpensansSemibold16)
                                .foregroundColor(Color.potSoftSecondary)
                            }
                            .hidden()
                        }
                        
                        VStack (alignment: .leading, spacing: 0){
                            Text("Proses Akhirnya")
                                .potFont(.OpensansSemibold20)
                                .padding(.bottom, 8)
                            Text("Akhirnya proses ini berakhir juga pada akhirnya.")
                                .potFont(.OpensansRegular16)
                        }
                        .padding(.leading, 46)
                        .padding(.top, 5)
                    }
                    .opacity(0.5)
                }
            }
            .padding(.horizontal)
        }
        .navSpacer()
    }
}

struct StepByStep_Previews: PreviewProvider {
    static var previews: some View {
        StepByStep()
    }
}
