//
//  Style.swift
//  PotionUI
//
//  Created by Damar Paramartha on 19/11/20.
//

import SwiftUI

struct Style: View {
    var body: some View {
        Text("Hello, World!")
            .padding()
            .background(Color.potSoftSuccess)
            .cornerRadius(radius: 8, corners: [.topLeft, .topRight])
            .padding()
    }
}
public struct CornerRadiusStyle: ViewModifier {
    internal init(radius: CGFloat, corners: UIRectCorner) {
        self.radius = radius
        self.corners = corners
    }
    
    var radius: CGFloat
    var corners: UIRectCorner
    
    struct CornerRadiusShape: Shape {
        
        var radius = CGFloat.infinity
        var corners = UIRectCorner.allCorners
        
        func path(in rect: CGRect) -> Path {
            let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            return Path(path.cgPath)
        }
    }
    
    public func body(content: Content) -> some View {
        content
            .clipShape(CornerRadiusShape(radius: radius, corners: corners))
    }
}

public extension View {
    func cornerRadius(radius: CGFloat, corners: UIRectCorner) -> some View {
        ModifiedContent(content: self, modifier: CornerRadiusStyle(radius: radius, corners: corners))
    }
}

// Untuk cek size
extension UIScreen{
    public static let screenWidth = UIScreen.main.bounds.size.width
    public static let screenHeight = UIScreen.main.bounds.size.height
    public static let screenSize = UIScreen.main.bounds.size
}

struct Style_Previews: PreviewProvider {
    static var previews: some View {
        Style()
            .previewLayout(.sizeThatFits)
    }
}
