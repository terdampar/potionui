//
//  Input.swift
//  PotionUI
//
//  Created by Damar Paramartha on 03/02/21.
//

import SwiftUI

struct InputText: View {
    @State private var username : String = ""
    @State private var isUsernameError : Bool = false
    @State private var password : String = ""
    @State private var isPasswordError : Bool = false
    
//    @State private var longtext : String = "tes"
    
    var body: some View {
        VStack {
            AppBar(title: "Input Text")
            VStack (alignment: .leading, spacing: 8){
                Text("Username")
                    .potFont(.OpensansRegular16)
                potTextField(
                    image: Image(systemName: "person.fill"),
                    placeholder: "Placeholder",
                    text: $username,
                    isError: $isUsernameError,
                    errorMessage: "Something went wrong."
                )
                Text("Password")
                    .potFont(.OpensansRegular16)
                potSecureField(
                    image: Image(systemName: "lock.fill"),
                    placeholder: "••••••",
                    text: $password,
                    isError: $isPasswordError,
                    errorMessage: "Invalid password."
                )
//                potTextEditor(text: $longtext)
                
                Spacer()
                HStack {
                    potButton(text: "TextField", style: .fill, textColor: .white, color: .potPrimary, action: {isUsernameError.toggle()
                    })
                    potButton(text: "SecureField", style: .fill, textColor: .white, color: .potPrimary, action: {isPasswordError.toggle()
                    })
                }
            }
            .padding()
        }
        .navSpacer()
    }
}

public struct potTextField: View {
    public init(image: Image? = nil, placeholder: String? = nil, text: Binding<String>, isError: Binding<Bool>, errorMessage: String) {
        self.image = image
        self.placeholder = placeholder
        self._text = text
        self._isError = isError
        self.errorMessage = errorMessage
    }
    
    var image : Image?
    var placeholder : String?
    @Binding var text : String
    @Binding var isError : Bool
    var errorMessage : String
    
    public var body: some View{
        VStack (alignment: .leading){
            ZStack(alignment: .bottom) {
                HStack (spacing: 8){
                    image
                        .foregroundColor(.potSuccess)
                    TextField(placeholder ?? "", text: $text)
                        .autocapitalization(.none)
                        .disableAutocorrection(true)
                    Spacer()
                    potButton(
                        image: Image(systemName: "xmark"),
                        style: .ghost,
                        textColor: .potSecondary,
                        action: {
                            text = ""
                            isError = false
                        })
                }
                .frame(height: 24)
                .potFont(.OpensansRegular16)
                .padding()
                .background(Color.potLight)
                .cornerRadius(4.0)
                Rectangle()
                    .frame(height: 2)
                    .foregroundColor(.potDanger)
                    .cornerRadius(radius: 8, corners: [.bottomLeft, .bottomRight])
                    .opacity(isError ? 1 : 0)
                    .animation(.easeInOut(duration: 0.15))
            }
            Text(errorMessage)
                .potFont(.OpensansRegular14, color: .potDanger)
                .opacity(isError ? 1 : 0)
                .animation(.easeInOut(duration: 0.18))
        }
    }
}

public struct potSecureField: View {
    public init(image: Image? = nil, placeholder: String? = nil, text: Binding<String>, isError: Binding<Bool>, errorMessage: String) {
        self.image = image
        self.placeholder = placeholder
        self._text = text
        self._isError = isError
        self.errorMessage = errorMessage
    }
    
    var image : Image?
    var placeholder : String?
    @Binding var text : String
    @Binding var isError : Bool
    var errorMessage : String
    
    @State private var isShowing: Bool = false
    
    public var body: some View{
        VStack (alignment: .leading){
            ZStack(alignment: .bottom) {
                HStack (alignment: .center,spacing: 8){
                    image
                        .foregroundColor(.potSuccess)
                    ZStack (alignment: .center){
                        if !isShowing{
                            SecureField(placeholder ?? "", text: $text)
                        }else{
                            TextField(placeholder ?? "" , text: $text)
                                .autocapitalization(.none)
                                .disableAutocorrection(true)
                        }
                    }
                    Spacer()
                    potButton(
                        image:(!isShowing ? Image(systemName: "eye") : Image(systemName: "xmark")),
                        style: .ghost,
                        textColor: .potSecondary,
                        action: {
                            if (isShowing){
                                text = ""
                                isShowing.toggle()
                            }else {
                                isShowing.toggle()
                            }
                        })
                }
                .frame(height: 24)
                .potFont(.OpensansRegular16)
                .padding()
                .background(Color.potLight)
                .cornerRadius(4.0)
                Rectangle()
                    .frame(height: 2)
                    .foregroundColor(.potDanger)
                    .cornerRadius(radius: 8, corners: [.bottomLeft, .bottomRight])
                    .opacity(isError ? 1 : 0)
                    .animation(.easeInOut(duration: 0.15))
            }
            Text(errorMessage)
                .potFont(.OpensansRegular14, color: .potDanger)
                .opacity(isError ? 1 : 0)
                .animation(.easeInOut(duration: 0.18))
        }
    }
}

public struct potTextEditor: View{
    //TIDAK BISA GANTI WARNA BACKGROUND
    @Binding var text: String
    
    public var body: some View {
        VStack{
            TextEditor(text: $text)
                .padding()
                .potFont(.OpensansRegular16)
                .background(Color.potSuccess)
        }
    }
}

struct InputText_Previews: PreviewProvider {
    static var previews: some View {
        InputText()
    }
}
