//
//  Shimmer.swift
//  PotionUI
//
//  Created by Damar Paramartha on 14/12/20.
//

import SwiftUI

struct Shimmer: View {
    var body: some View {
        VStack {
            CardShimmer()
        }
    }
}

struct CardShimmer: View {
    @State var show = false
    var center = (UIScreen.screenWidth/2) + 100
    
    var body: some View {
        ZStack {
            Color.potLight
                .frame(height: 200)
            Color.white
                .frame(height: 200)
                .mask(
                    Rectangle()
                        .fill(
                            LinearGradient(gradient: Gradient(colors: [Color.clear, Color.white.opacity(0.5),.clear]), startPoint: .top, endPoint: .bottom)
                        )
                        .offset(x: self.show ? center : -center)
                )
        }
        .onAppear{
            withAnimation(Animation.default.speed(0.16).delay(0).repeatForever(autoreverses: false)){
                self.show.toggle()
            }
        }
    }
}

struct Shimmer_Previews: PreviewProvider {
    static var previews: some View {
        Shimmer()
    }
}
