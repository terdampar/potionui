//
//  Card.swift
//  PotionUI
//
//  Created by Damar Paramartha on 19/11/20.
//

import SwiftUI

struct Card: View {
    
    
    var body: some View {
        VStack {
            ScrollView {
                VStack(alignment: .leading) {
                    AppBarLarge(title: "Card")
                    ScrollView(.horizontal, showsIndicators: false){
                        LazyHStack(spacing: 16) {
                            VerticalCard(image: Image("artwork", bundle: .module), text: "Menu 2", desc: "Product Description and more", action: {}, style: .border)
                            VerticalCard(image: Image("artwork", bundle: .module), text: "Menu 2", desc: "Product Description and more", action: {})
                            VerticalCard(image: Image("artwork", bundle: .module), text: "Menu 2", desc: "Product Description and more", action: {})
                            VerticalCard(image: Image("artwork", bundle: .module), text: "Mina", desc: "Mina mina minari mina mina minari mina mina minari", action: {}, style: .border)
                        }
                        .padding(.horizontal, 16)
                    }
                    HorizontalCard(
                        image: Image("artwork", bundle: .module),
                        title: "Product Name",
                        desc: "You're gonna say more more more more more and more I wanna have more more more more more and more more more more and more",
                        action: {})
                    HorizontalCard(
                        image: Image("artwork", bundle: .module),
                        title: "Product Name",
                        desc: "You're gonna say more more more more more and more I wanna have more more more more more and more more more more and more",
                        action: {},
                        style: .border)
                    LargeCard(image: Image("artwork", bundle: .module), text: "Menu 1", action: {})
                }
            }
        }
        .navSpacer()
    }
}
public struct HorizontalCard: View {
    public init(image: Image, title: String, desc: String, action: @escaping () -> Void, style: HorizontalCard.Style? = .shadow) {
        self.image = image
        self.title = title
        self.desc = desc
        self.action = action
        self.style = style
    }
    
    var image: Image
    var title: String
    var desc: String
    var action: () -> Void
    
    public enum Style {
        case shadow, border
    }
    var style: Style? = .shadow
    
    public var body: some View {
        Button(action: action) {
            HStack(alignment: .top, spacing: 0) {
                image
                    .resizable()
                    .scaledToFill()
                    .frame(width: 96, height: 96)
                    .cornerRadius(8)
                    .padding(8)
                
                VStack (alignment: .leading, spacing: 0) {
                    Text(title)
                        .potFont(.OpensansBold16)
                        .foregroundColor(.potBW)
                    Text(desc)
                        .potFont(.OpensansRegular14)
                        .foregroundColor(.potSecondary)
                    
                }
                .padding(8)
                .frame(maxHeight: 96)
                Spacer()
            }
        }
        .background(Color.potSoftLight)
        .cornerRadius(8)
        .overlay(
            style == .border ?
            RoundedRectangle(cornerRadius: 8)
                .stroke(Color.potBW.opacity(0.38), lineWidth: 0.5) : nil
        )
        .padding(16)
        .shadow(color: style == .shadow ? Color.black.opacity(0.1) : Color.clear, radius: 24, x: 0, y: 8)
        
    }
}

public struct LargeCard: View {
    public init(image: Image, text: String, action: @escaping () -> Void) {
        self.image = image
        self.text = text
        self.action = action
    }
    
    var image: Image
    var text: String
    var action: () -> Void
    
    public var body: some View {
        Button(action: action) {
            VStack(alignment: .trailing, spacing: 0) {
                image
                    .resizable()
                    .scaledToFill()
                HStack (alignment: .lastTextBaseline) {
                    Text(text)
                    Image(systemName: "arrow.forward")
                }
                .potFont(.OpensansBold16)
                .foregroundColor(.potBW)
                .padding()
            }
        }
        .background(Color.potSoftLight)
        .cornerRadius(16)
        .padding(16)
        .shadow(color: Color.black.opacity(0.1), radius: 24, x: 0, y: 8)
    }
}

public struct VerticalCard: View {
    public init(image: Image, text: String, desc: String, action: @escaping () -> Void, style: VerticalCard.Style? = .shadow) {
        self.image = image
        self.text = text
        self.desc = desc
        self.action = action
        self.style = style
    }
    
    var image: Image
    var text: String
    var desc: String
    var action: () -> Void
    
    public enum Style {
        case shadow, border
    }
    var style: Style? = .shadow
    
    public var body: some View {
        VStack (spacing: 0){
            Button(action: action) {
                VStack(alignment: .leading, spacing: 0) {
                    image
                        .resizable()
                        .scaledToFill()
                        .frame(width: 120, height: 120)
                        .cornerRadius(16)
                        .padding(.bottom)
                    
                        Text(text)
                            .potFont(.OpensansBold16)
                            .foregroundColor(.potBW)
                        Text(desc)
                            .potFont(.OpensansRegular14)
                            .foregroundColor(.potSecondary)
                }
                .frame(width: 120)
                .padding(8)
            }
            .background(Color.potSoftLight)
            .cornerRadius(16)
            .overlay(
                style == .border ?
                RoundedRectangle(cornerRadius: 16)
                    .stroke(Color.potBW.opacity(0.38), lineWidth: 0.5) : nil
            )
            .shadow(color: style == .shadow ? Color.black.opacity(0.1) : Color.clear, radius: 24, x: 0, y: 8)
        }
        .padding(.vertical, 24)
    }
}



struct Card_Previews: PreviewProvider {
    static var previews: some View {
        Card()
            .preferredColorScheme(.dark)
    }
}
