//
//  More.swift
//  PotionUI
//
//  Created by Damar Paramartha on 19/11/20.
//

import SwiftUI

public struct MoreView <Content : View> : View {
    var content : Content
    
    public init(@ViewBuilder content: () -> Content) {
        self.content = content()
    }
    
    
    public var body: some View {
        ZStack {
            
            VStack(alignment: .center, spacing: 0){
                Spacer()
                // indicator if it could be dragged
                //                HStack(alignment: .center){
                //                    Spacer()
                //                    Rectangle()
                //                        .frame(width: 48, height: 5, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                //                        .cornerRadius(8.0)
                //                        .foregroundColor(Color.potBW.opacity(0.16))
                //                    Spacer()
                //                }
                //                .padding(.top,16)
                //                .background(Color.potSoftLight)
                
                VStack (spacing: 0){
                    content
                }
                .padding( 16)
                .background(Color.potSoftLight)
                .cornerRadius(radius: 8, corners: [.topLeft, .topRight])
            }
            .animation(.easeInOut(duration: 0.3))
        }
        .ignoresSafeArea()
    }
}

struct ViewMore: View {
    
    @Binding var isPresented : Bool
    
    var body: some View {
        ZStack {
            Rectangle()
                .foregroundColor(Color.potBW.opacity(0.2))
                .ignoresSafeArea(.all)
                .opacity(isPresented ? 1 : 0)
                .onTapGesture {
                    self.isPresented.toggle()
                }
                .animation(.easeInOut(duration: 0.3))
            VStack{
                MoreView{
                    ButtonList(action: {}, icon: Image(systemName: "pencil"), text: "Edit", color: Color.potSecondary )
                    ButtonList(action: {}, icon: Image(systemName: "trash"), text: "Delete", color: Color.potDanger )
                    potButton(
                        text: "Cancel",
                        style: .fill,
                        textColor: .potBW,
                        color: Color.potLight,
                        action: {
                            isPresented.toggle()}
                    )
                    .padding(.vertical)
                }
            }
            .offset(y: self.isPresented ? 0 : UIScreen.screenHeight)
        }
    }
}

struct More: View {
    
    @State var isPresented : Bool = false
    
    var body: some View {
        VStack{
            Button("View More"){
                isPresented.toggle()
            }
            ViewMore(isPresented: $isPresented)
        }
    }
}


struct More_Previews: PreviewProvider {
    static var previews: some View {
        //MoreItem()
        //.previewLayout(.sizeThatFits)
        More()
            .preferredColorScheme(.dark)
        
        
        
        
    }
}
