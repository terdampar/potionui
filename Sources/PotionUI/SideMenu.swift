//
//  SideMenu.swift
//  PotionUI
//
//  Created by Damar Paramartha on 03/12/20.
//

import SwiftUI

struct SideView <Content : View> : View {
    var content : Content
    
    init(@ViewBuilder content: () -> Content) {
        self.content = content()
    }
    
    @State private var isPresented : Bool = true
    
    var body: some View {
        ZStack {
            Text("Open Slide Menu")
                .onTapGesture {
                    self.isPresented.toggle()
                }
            Rectangle()
                    .foregroundColor(Color.black.opacity(0.38))
//                    .ignoresSafeArea(.all)
                    .opacity(isPresented ? 1 : 0)
                    .onTapGesture {
                        self.isPresented.toggle()
                    }
                    .animation(.easeInOut(duration: 0.38))
            HStack {
                VStack(alignment: .leading, spacing: 0){
                    VStack (spacing: 0){
                        HStack {
                            Text("Static stuff")
                                .potFont(.Montserrat32)
                        }
                        .background(Color.potLight)
                        .padding(.top, 24)

                        
                        ScrollView {
                            content
                                .padding( 16)
                        }
                        
                        
                    }
                    .frame(width: UIScreen.screenWidth-120, height: UIScreen.screenHeight)
                    .background(Color.potSoftLight)
                }
                .offset(x: self.isPresented ? 0 : -UIScreen.screenWidth)
                .animation(.easeInOut(duration: 0.38))
                Spacer()
            }
        }
        .ignoresSafeArea()
    }
}

struct SideMenu: View {
    var body: some View {
        SideView{
            ButtonList(action: {}, text: "Edit", color: Color.potInfo )
            ButtonList(action: {}, icon: Image(systemName: "trash"), text: "Edit", color: Color.potDanger )
            Spacer()
            ButtonList(action: {},icon: Image(systemName: "escape"), text: "Logout", color: .potDanger)
        }
    }
}

struct SideMenu_Previews: PreviewProvider {
    static var previews: some View {
        SideMenu()
            .preferredColorScheme(.dark)
            
    }
}
