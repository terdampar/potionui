//
//  FontStyle.swift
//  PotionUI
//
//  Created by Damar Paramartha on 22/10/20.
//

import SwiftUI

public func registerFonts() {
    _ = UIFont.registerFont(bundle: .module, fontName: "Montserrat-Bold", fontExtension: "ttf")
    _ = UIFont.registerFont(bundle: .module, fontName: "Montserrat-Regular", fontExtension: "ttf")
    _ = UIFont.registerFont(bundle: .module, fontName: "Montserrat-SemiBold", fontExtension: "ttf")
    _ = UIFont.registerFont(bundle: .module, fontName: "OpenSans-Bold", fontExtension: "ttf")
    _ = UIFont.registerFont(bundle: .module, fontName: "OpenSans-Regular", fontExtension: "ttf")
    _ = UIFont.registerFont(bundle: .module, fontName: "OpenSans-SemiBold", fontExtension: "ttf")
}

extension UIFont {
    static func registerFont(bundle: Bundle, fontName: String, fontExtension: String) -> Bool {

        guard let fontURL = bundle.url(forResource: fontName, withExtension: fontExtension) else {
            fatalError("Couldn't find font \(fontName)")
        }

        guard let fontDataProvider = CGDataProvider(url: fontURL as CFURL) else {
            fatalError("Couldn't load data from the font \(fontName)")
        }

        guard let font = CGFont(fontDataProvider) else {
            fatalError("Couldn't create font from data")
        }

        var error: Unmanaged<CFError>?
        let success = CTFontManagerRegisterGraphicsFont(font, &error)
        guard success else {
            print("Error registering font: maybe it was already registered.")
            return false
        }

        return true
    }
}

//MARK: CUSTOM MODIFIER
public struct PotionTypo: ViewModifier {
    
    
    public enum Style {
        
        /// Montserrat
        case Montserrat32, Montserrat24, Montserrat20, Montserrat16
        
        /// Open Sans
        case OpensansSemibold20, OpensansBold16, OpensansSemibold16, OpensansRegular16, OpensansRegular14
    }
    
    var style: Style
    
    public func body(content: Content) -> some View {
        registerFonts()
        switch style {
        case .Montserrat32: return content
            .font(Font.custom("Montserrat-Bold", size: 32))
        case .Montserrat24: return content
            .font(Font.custom("Montserrat-Bold", size: 24))
        case .Montserrat20: return content
            .font(Font.custom("Montserrat-Bold", size: 20))
        case .Montserrat16: return content
            .font(Font.custom("Montserrat-SemiBold", size: 16))
            
        case .OpensansSemibold20: return content
            .font(Font.custom("OpenSans-SemiBold", size: 20))
        case .OpensansBold16: return content
            .font(Font.custom("OpenSans-Bold", size: 16))
        case .OpensansSemibold16: return content
            .font(Font.custom("OpenSans-Bold", size: 16))
        case .OpensansRegular16: return content
            .font(Font.custom("OpenSans-Regular", size: 16))
        case .OpensansRegular14: return content
            .font(Font.custom("OpenSans-Regular", size: 14))
        }
    }
}//: CUSTOM MODIFIER

public extension View {
    func potFont(_ style: PotionTypo.Style) -> some View {
        self
            .modifier(PotionTypo(style: style))
    }
    
    func potFont(_ style: PotionTypo.Style, color: Color) -> some View {
        self
            .modifier(PotionTypo(style: style))
            .foregroundColor(color)
    }
    
}
//MARK: Custom Modifier END

struct Typography: View {
    var body: some View {
        VStack (spacing :0) {
            AppBar(title: "Typography")
            
            ScrollView {
                KindsOfTypography()
                
                Spacer()
            }
        }
        .navSpacer()
    }
}

struct KindsOfTypography: View {
    var body: some View {
        VStack (alignment: .leading, spacing: 8){
            Text("Montserrat32 - App Title")
                .potFont(.Montserrat32)
            Text("Montserrat24 - Title")
                .potFont(.Montserrat24)
            Text("Montserrat20 - Modal Title")
                .potFont(.Montserrat20)
            Text("Montserrat16 - Card Title")
                .potFont(.Montserrat16)
            Text("OpensansSemibold20 - Pop Up Title")
                .potFont(.OpensansSemibold20)
            Text("OpensansSemibold16 - List Title/Sub Title/Text Button")
                .potFont(.OpensansSemibold16)
            Text("OpensansBold16 - Shape Button (Fill/Outline)")
                .potFont(.OpensansBold16)
            Text("OpensansRegular16 - Body")
                .potFont(.OpensansRegular16)
            Text("OpensansRegular14 - List Body")
                .potFont(.OpensansRegular14)
        }
    }
}


struct Typography_Previews: PreviewProvider {
    static var previews: some View {
        Typography()
    }
}
