//
//  Navigation.swift
//  PotionUI
//
//  Created by Damar Paramartha on 14/12/20.
//

import SwiftUI

struct Navigation: View {
    var body: some View {
        NavigationView {
            VStack (spacing: 16) {
                ListNav(text: "Ke Menu")
                AppBar(title: "Judul Menu", backButton: .show)
                AppBar(title: "Judul Menu", backButton: .hide)
                AppBarLarge(title: "Judul Menu", backButton: .hide)
                AppBarLarge(title: "Judul Menu")
            }
            .navSpacer()
        }
    }
}

struct ListNav: View {
    var text : String
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0){
            HStack (alignment: .firstTextBaseline, spacing: nil){
                Text(text)
                    .potFont(.OpensansRegular16)
                Spacer()
                Image(systemName: "chevron.forward")
            }
            .foregroundColor(.potBW)
            .padding(.vertical, 16)
            Divider()
        }
        .padding(.horizontal, 16)
    }
}

public struct AppBar: View{
    public init(title: String, backButton: AppBar.Back = .show) {
        self.title = title
        self.backButton = backButton
    }
    
    var title: String
    public enum Back{
        case show, hide
    }
    var backButton: Back = .show
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    
    public var body: some View{
        VStack {
            HStack{
                if(backButton == .show){
                    
                    Button(action : {self.mode.wrappedValue.dismiss()}) {
                        Image(systemName: "arrow.backward")
                            .potFont(.OpensansBold16)
                            .foregroundColor(Color.potBW)
                            .frame(width: 16, height: 16)
                    }
                    
                }
                Spacer()
                Text(title)
                    .potFont(.Montserrat16)
                Spacer()
                Rectangle()
                    .frame(width: 16, height: 16)
                    .foregroundColor(.clear)
            }
            .padding(.top, 24)
            .padding(16)
            .background(Color.potLight)
        }
    }
}

//struct CustomAppBar: View{
//    var title: String
//    
//    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
//    
//    var body: some View{
//        VStack {
//            HStack{
//                    Button(action : {self.mode.wrappedValue.dismiss()}) {
//                        Image(systemName: "arrow.backward")
//                            .potFont(.OpensansBold16)
//                            .foregroundColor(Color.potBW)
//                            .frame(width: 16, height: 16)
//                    }
//                    
//                Spacer()
//                Text(title)
//                    .potFont(.Montserrat16)
//                Spacer()
//                Rectangle()
//                    .frame(width: 16, height: 16)
//                    .foregroundColor(.clear)
//            }
//            .padding(.top, 24)
//            .padding(16)
//            .background(Color.potLight)
//        }
//    }
//}

public struct AppBarLarge: View{
    public init(title: String, backButton: AppBarLarge.Back = .show) {
        self.title = title
        self.backButton = backButton
    }
    
    var title: String
    public enum Back{
        case show, hide
    }
    var backButton: Back = .show
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    
    public var body: some View{
        VStack (alignment: .leading) {
            HStack{
                if(backButton == .show){
                    Button(action : {self.mode.wrappedValue.dismiss()}) {
                        Image(systemName: "arrow.backward")
                            .potFont(.OpensansBold16)
                            .foregroundColor(Color.potBW)
                            .frame(width: 16, height: 16)
                    }
                }
                Spacer()
            }
            .padding(.bottom)
            Text(title)
                .potFont(.Montserrat32)
        }
        .padding(.top, 24)
        .padding(16)
        .background(Color.potLight)
        
    }
}

//MARK: Hide back button without disabling back gesture
extension UINavigationController: UIGestureRecognizerDelegate {
    override open func viewDidLoad() {
        super.viewDidLoad()
        interactivePopGestureRecognizer?.delegate = self
    }
    
    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return viewControllers.count > 1
    }
}

//MARK: Extension for Nav Spacer
public struct SpacerForNav: ViewModifier {
    public func body(content: Content) -> some View {
        content
            .ignoresSafeArea(edges: .top)
            .navigationTitle("")
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
        
    }
}
public extension View {
    func navSpacer() -> some View {
        self.modifier(SpacerForNav())
    }
}



//Preview
struct Navigation_Previews: PreviewProvider {
    static var previews: some View {
        Navigation()
    }
}
