//
//  Info.swift
//  PotionUI
//
//  Created by Damar Paramartha on 28/01/21.
//

import SwiftUI

struct InfoView <Content : View> : View {
    var content : Content
    
    init(@ViewBuilder content: () -> Content) {
        self.content = content()
    }
    
    
    var body: some View {
        ZStack {
            
            VStack(alignment: .center, spacing: 0){
                Spacer()
                VStack (spacing: 0){
                    content
                }
                .padding(16)
                .padding(.bottom)
                .background(Color.potSoftLight)
                .cornerRadius(radius: 8, corners: [.topLeft, .topRight])
            }
            .animation(.easeInOut(duration: 0.3))
        }
        .ignoresSafeArea()
    }
}

public struct ViewInfo: View {
    public init(isPresented: Binding<Bool>, title: String, description: String) {
        self._isPresented = isPresented
        self.title = title
        self.description = description
    }
    
    @Binding var isPresented : Bool
    var title: String
    var description: String
    
    public var body: some View {
        ZStack {
            Rectangle()
                .foregroundColor(Color.potBW.opacity(0.2))
                .ignoresSafeArea(.all)
                .opacity(isPresented ? 1 : 0)
                .onTapGesture {
                    self.isPresented.toggle()
                }
                .animation(.easeInOut(duration: 0.3))
            VStack{
                InfoView{
                    VStack (alignment: .leading){
                        HStack (alignment: .center) {
                            Label(title, systemImage: "info.circle")
                                .potFont(.Montserrat20, color: .potBW)
                                .padding(.vertical)
                            Spacer()
                            potButton(image: Image(systemName: "xmark"), style: .ghost, textColor: .potSecondary, action:  {isPresented.toggle()})
                        }
                        Text(description)
                            .potFont(.OpensansRegular16, color: .potSecondary)
                    }
                }
            }
            .offset(y: self.isPresented ? 0 : UIScreen.screenHeight)
        }
    }
}

public struct Info: View {
    
    @State private var isPresented : Bool = false
    
    public var body: some View {
        ZStack {
            VStack (alignment: .leading, spacing: 0){
                AppBar(title: "Info", backButton: .show)
                VStack (alignment: .leading) {
                    Text("Berguna untuk meningkatkan pengetahuan atau kemampuan pengguna, Mengurangi ketidakpastian dalam proses pengambilan keputusan, Menggambarkan keadaan sesuatu hal atau peristiwa yang terjadi.")
                        .padding(.bottom)
                    potButton(text: "Apa itu Informasi?", image: Image(systemName: "info.circle"), style: .ghost, textColor: .potSecondary, action: {isPresented.toggle()})
                }
                .padding()
                    Spacer()
            }
            .navSpacer()
            ViewInfo(
                isPresented: $isPresented,
                title: "Apa itu Info",
                description: "Informasi adalah pesan atau kumpulan pesan yang terdiri dari order sekuens dari simbol, atau makna yang dapat ditafsirkan dari pesan atau kumpulan pesan. Informasi dapat direkam atau ditransmisikan. Hal ini dapat dicatat sebagai tanda-tanda, atau sebagai sinyal berdasarkan gelombang."
            )
            
        }
    }
}

struct Info_Previews: PreviewProvider {
    static var previews: some View {
        Info()
    }
}
