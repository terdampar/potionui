//
//  ToggleTokens.swift
//  PotionUI
//
//  Created by Damar Paramartha on 08/02/21.
//

import SwiftUI

struct ToggleTokens: View {
    
    let tokens = [
        Tokens(text: "Nasi Goreng", active: false),
        Tokens(text: "Soto", active: false),
        Tokens(text: "Sate Ayam", active: false),
        Tokens(text: "Siomay", active: false),
        Tokens(text: "Ayam Geprek", active: false),
        Tokens(text: "KFC", active: false)
    ]
    
    var columns : [GridItem] = [
        GridItem(.adaptive(minimum: 124, maximum: 150))
    ]
    
    var body: some View {
        VStack {
            AppBar(title: "Toggle Tokens")
            VStack {
                ScrollView() {
                    LazyVGrid(columns: columns, spacing: 16){
                        ForEach (0..<tokens.count){ value in
                            Tokens(text: tokens[value].text, active: tokens[value].active)
                        }
                        
                        
                    }
                    Spacer()
                }
            }
            .padding(.horizontal)
        }
        .navSpacer()
    }
}

public struct Tokens: View {
    public init(text: String, active: Bool) {
        self.text = text
        self.active = active
    }
    
    var text: String
    @State var active: Bool = false
    
    public var body: some View {
        VStack {
            Text(text)
                .lineLimit(1)
                .potFont(.OpensansRegular16)
                .padding(.horizontal)
                .frame(width: .infinity + 16, height: 56, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                .background(active ? Color.potSoftSuccess : Color.potLight)
                .cornerRadius(36)
                .animation(.easeInOut(duration: 0.2))
                .onTapGesture {
                    active.toggle()
                }
        }
    }
}

struct ToggleTokens_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ToggleTokens()
        }
    }
}
