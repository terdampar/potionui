import XCTest

import PotionUITests

var tests = [XCTestCaseEntry]()
tests += PotionUITests.allTests()
XCTMain(tests)
