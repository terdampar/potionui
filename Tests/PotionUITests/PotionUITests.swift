import XCTest
@testable import PotionUI

final class PotionUITests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(PotionUI().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
